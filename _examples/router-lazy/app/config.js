"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CONFIG = {
    baseUrls: {
        characters: 'api/characters.json',
        vehicles: 'api/vehicles.json'
    }
};
//# sourceMappingURL=config.js.map