"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Character = /** @class */ (function () {
    function Character(id, name, side) {
        this.id = id;
        this.name = name;
        this.side = side;
    }
    return Character;
}());
exports.Character = Character;
//# sourceMappingURL=character.js.map