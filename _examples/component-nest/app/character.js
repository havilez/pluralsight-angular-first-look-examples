"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Character = /** @class */ (function () {
    function Character(id, name) {
        this.id = id;
        this.name = name;
    }
    return Character;
}());
exports.Character = Character;
//# sourceMappingURL=character.js.map